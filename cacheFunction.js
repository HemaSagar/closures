function cacheFunction(cb) {
    const cache = {};
    return function () {

        let args = JSON.stringify([...arguments]);
        if (args in cache) {
            // console.log("from cache")
            return cache[args];


        }

        else{
            // console.log('from cb');
            return cache[args] = cb(...arguments);
        }
    }
}




module.exports = cacheFunction
