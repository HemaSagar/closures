const limitFunctionCallCount = require('../limitFunctionCallCount.js')

// asssigning the returned function which limits the cb invokes to anothe variable.
const lfcc = limitFunctionCallCount(() => {
    console.log("calling cb only a certain number of times");
},5);

// calling the cb function more than n no.of times to test the result, it should only be called n no.of times
for(let i = 0; i<10; i++){
    lfcc();
}