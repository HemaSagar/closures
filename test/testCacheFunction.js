const cacheFunction = require('../cacheFunction.js');

const returnedFunction = cacheFunction(function cb(){
    const sum = 0;
    return [...arguments].reduce((prev,curr) => {
        return prev+curr;
    });
})

console.log(returnedFunction(1,2));
console.log(returnedFunction(1,2,3,4,5));