const counterFactory = require('../counterFactory.js')

let counter = 5;
console.log(counterFactory().increment(counter));
console.log(counterFactory().decrement(counter));
