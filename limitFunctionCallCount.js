function limitFunctionCallCount(cb, n){
    return () =>{
        if(n>0){
            n--;
            cb();
        }
    
    return null;
    }
}


module.exports = limitFunctionCallCount;

