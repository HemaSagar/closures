function counterFactory(){
    return {
        increment: function (counter) {
            return counter+1;

        },

        decrement: function (counter) {
            return counter-1;
        }
    }

}

module.exports = counterFactory;